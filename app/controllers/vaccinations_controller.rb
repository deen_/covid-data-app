# frozen_string_literal: true

class VaccinationsController < ApplicationController
  rescue_from ActionController::ParameterMissing, with: :require_params
  before_action :authenticate_user!, except: %i[index search]
  before_action :set_vaccination, only: %i[index new create search]
  before_action :vaccination_params, only: %i[create]
  before_action :search_params, only: %i[search]
  before_action :set_data, only: %i[index search]

  def index; end

  def new; end

  def create
    if upload_csv_to_local
      AddVaccination.perform_async(@csv_path)
      redirect_to root_path
    else
      render :new
    end
  end

  def destroy
    # delete all uploaded csv files
    CsvUpload.delete_all
    ActiveStorage::Attachment.all.each(&:purge)

    RemoveVaccination.perform_async
  end

  def search
    @search_vaccination = SearchVaccination.new(params[:vaccination])
    @vaccinations = @search_vaccination.call
    # set_search_params
    render :index
  end

  private

  def set_vaccination
    @vaccination = Vaccination.new
    @vaccination_vaccine = @vaccination.vaccination_vaccines.build
  end

  def vaccination_params
    params.require(:vaccination).permit(:file)
  end

  def search_params
    params.require(:vaccination).permit(:country, :data_source, :date_updated, :vaccines,
                                        :first_vaccine_date, vaccination_vaccine: [:vaccine])
  end

  def require_params
    flash[:alert] = "The file field is required."
    redirect_to new_vaccinations_path
  end

  def upload_csv_to_local
    file = params[:vaccination][:file]

    return false unless csv?(file)

    vac = CsvUpload.create!(user_id: current_user.id)
    vac.csv.attach(file)
    @csv_path = vac.csv.url
  rescue URI::InvalidURIError
    # do nothing
  end

  def csv?(file)
    if File.extname(file) == ".csv"
      true
    else
      flash[:alert] = "Please upload a valid CSV file"
      false
    end
  end

  def set_data
    @vaccinations = Vaccination.all
    @countries = Country.order(:name).all
    @data_sources = Vaccination.data_sources
    @vaccines = Vaccine.order(:name).all
    @search_vaccination = SearchVaccination.new(nil)
  end
end
