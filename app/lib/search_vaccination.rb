class SearchVaccination
  attr_accessor :vaccinations, :country, :data_source, :date_updated, :first_vaccine_date, :vaccine

  def initialize(filter)
    self.vaccinations = Vaccination.all

    return unless filter

    self.country = filter[:country]
    self.data_source = filter[:data_source]
    self.date_updated = filter[:date_updated]
    self.first_vaccine_date = filter[:first_vaccine_date]
    self.vaccine = filter[:vaccination_vaccine][:vaccine] if filter[:vaccination_vaccine].present?
  end

  def call
    self.vaccinations = vaccinations.where(country_id: country) if country.present?
    self.vaccinations = vaccinations.where(data_source: data_source) if data_source.present?

    search_by_date_updated
    search_by_first_date_vaccinated
    search_by_vaccine

    vaccinations
  end

  def search_by_vaccine
    return if vaccine.blank?

    self.vaccinations = vaccinations.joins(:vaccination_vaccines)
                                    .where(vaccination_vaccines: { vaccine_id: vaccine })
  end

  def search_by_date_updated
    return if date_updated.blank?

    range = date_updated.split(" to ")
    self.vaccinations = if range.size > 1
                          vaccinations.where("date_updated >= ? AND date_updated <= ?",
                                             range[0], range[1])
                        else
                          vaccinations.where(date_updated: date_updated)
                        end
  end

  def search_by_first_date_vaccinated
    return if first_vaccine_date.blank?

    range = first_vaccine_date.split(" to ")
    if range.size > 1
      self.vaccinations = vaccinations.where(
        "first_vaccine_date >= ? AND first_vaccine_date <= ?", range[0], range[1]
      )
    else
      self.vaccinations = vaccinations.where(first_vaccine_date: first_vaccine_date)
    end
  end
end
