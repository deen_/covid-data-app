# frozen_string_literal: true

class RemoveVaccination
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform
    Vaccination.destroy_all
    # ActionCable.server.broadcast "upload_channel",
    #                              { action: "delete", message: "Records were successfully deleted." }
  end
end
