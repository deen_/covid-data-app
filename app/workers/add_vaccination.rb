# frozen_string_literal: true

class AddVaccination
  require "open-uri"
  require "csv"

  include Sidekiq::Worker
  include ApplicationHelper
  sidekiq_options retry: false

  def perform(amazon_url)
    URI.parse(amazon_url).open do |file|
      # open(amazon_url) do |file|
      csv = CSV.new(file, headers: :first_row)
      csv.each do |row|
        # hash row and transform key to lowercase
        hash_row!(row)

        # retrieve vaccines
        retrieve_vaccines!

        # add and remove key/value pairs to match db fields
        format_hashed_row!

        # save
        save_vaccination!
      end
    end
    notify_job_done!
  end

  def retrieve_vaccines!
    @vaccines = []
    @vaccines = @hashed["vaccines_used"].split(",") if @hashed["vaccines_used"]
  end

  def get_country_by_iso3(iso3, country, who_region)
    Country.create_with(name: country, who_region: who_region).find_or_create_by(iso3: iso3).id
  end

  def get_vaccine_by_name(vaccine)
    Vaccine.find_or_create_by(name: vaccine).id
  end

  def hash_row!(row)
    @hashed = {}
    row.to_hash.each_pair do |k, v|
      @hashed.merge!({ k.downcase => v })
    end
  end

  def format_hashed_row!
    # add country_id
    @hashed[:country_id] =
      get_country_by_iso3(@hashed["iso3"], @hashed["country"], @hashed["who_region"])

    # remove country, iso3, who_region
    @hashed = @hashed.except("country", "iso3", "who_region", "vaccines_used")
  end

  def save_vaccination!
    row = Vaccination.new @hashed
    @vaccines.each do |v|
      row.vaccination_vaccines.build(vaccine_id: get_vaccine_by_name(v.strip))
    end

    errors.merge!(row.errors) unless row.save
  end

  def notify_job_done!
    # ActionCable.server.broadcast "upload_channel",
    #                              { action:  "create",
    #                                message: "A new vaccination data has been uploaded." }
  end
end
