# frozen_string_literal: true

class Vaccination < ApplicationRecord
  belongs_to :country
  has_many :vaccination_vaccines, dependent: :destroy

  enum data_source: { REPORTING: 0, OWID: 1 }

  validates_associated :country, :vaccination_vaccines
end
