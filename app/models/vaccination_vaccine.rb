# frozen_string_literal: true

class VaccinationVaccine < ApplicationRecord
  belongs_to :vaccination
  belongs_to :vaccine
end
