# frozen_string_literal: true

class CsvUpload < ApplicationRecord
  belongs_to :user
  has_one_attached :csv
end
