# frozen_string_literal: true

class Vaccine < ApplicationRecord
  has_many :vaccinations_vaccine, dependent: :destroy
end
