import consumer from "./consumer"

consumer.subscriptions.create("UploadChannel", {
  connected() {
    // Called when the subscription is ready for use on the server
  },

  disconnected() {
    // Called when the subscription has been terminated by the server
  },

  received(data) {
    document.querySelector("#success-alert").style.display = "block"
    document.querySelector("#success-alert-msg").innerHTML = data.message
  }
});