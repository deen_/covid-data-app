# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users, controllers: { registrations: 'users/registrations' }

  root to: 'vaccinations#index'
  resource :vaccinations, only: %i[new create destroy]
  post 'search_record', to: 'vaccinations#search'
  get 'search_record', to: 'vaccinations#index'

  mount ActionCable.server, at: '/cable'
end
