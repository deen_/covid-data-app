# frozen_string_literal: true

class CreateCountries < ActiveRecord::Migration[6.1]
  def change
    create_table :countries do |t|
      t.string :name
      t.string :iso3
      t.string :who_region
    end
  end
end
