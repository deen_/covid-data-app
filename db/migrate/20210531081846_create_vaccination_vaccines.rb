# frozen_string_literal: true

class CreateVaccinationVaccines < ActiveRecord::Migration[6.1]
  def change
    create_table :vaccination_vaccines do |t|
      t.references :vaccination, null: false, foreign_key: true
      t.references :vaccine, null: false, foreign_key: true
    end
  end
end
