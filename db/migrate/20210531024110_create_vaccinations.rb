# frozen_string_literal: true

class CreateVaccinations < ActiveRecord::Migration[6.1]
  def change
    create_table :vaccinations do |t|
      t.references :country, null: false, foreign_key: true
      t.integer :data_source
      t.date :date_updated
      t.integer :total_vaccinations
      t.integer :persons_vaccinated_1plus_dose
      t.float :total_vaccinations_per100
      t.float :persons_vaccinated_1plus_dose_per100
      t.date :first_vaccine_date
      t.integer :number_vaccines_types_used
    end
  end
end
