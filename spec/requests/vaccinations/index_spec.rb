require "rails_helper"

RSpec.describe "Vaccinations", type: :request do
  describe "GET /vaccinations" do
    it "renders vaccination data" do
      create_list :vaccination, 5
      get root_path
      expect(response).to have_http_status :ok
    end
  end
end
