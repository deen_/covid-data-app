require "rails_helper"

RSpec.describe "Vaccinations", type: :request do
  describe "DELETE /vaccinations" do
    let(:user) { create :user }
    before(:each) do
      create :csv_upload
    end

    context "as an authenticated user" do
      it "can delete vaccination data" do
        sign_in user

        delete vaccinations_path
        expect(CsvUpload.count).to eq 0
      end
    end

    context "as a guest" do
      it "cannot delete vaccination data" do
        delete vaccinations_path
        expect(CsvUpload.count).to_not eq 0
      end
    end
  end
end
