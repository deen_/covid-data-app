require "rails_helper"

RSpec.describe "Vaccinations", type: :request do
  describe "POST /vaccinations" do
    let(:user) { create :user }
    let(:vaccination_params) do
      { vaccination: { file: TestCSV.test_csv } }
    end

    context "as an authenticated user" do
      it "can upload vaccination data" do
        sign_in user

        expect do
          post vaccinations_path, params: vaccination_params
        end.to change(ActiveStorage::Attachment, :count).by(1)
        expect(CsvUpload.count).to eq 1
      end
    end

    context "as a guest" do
      it "cannot upload vaccination data" do
        expect do
          post vaccinations_path, params: vaccination_params
        end.to_not change(ActiveStorage::Attachment, :count)
        expect(CsvUpload.count).to eq 0
      end
    end
  end
end
