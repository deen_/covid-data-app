require "rails_helper"

RSpec.describe "User Registration", type: :request do
  describe "POST /users" do
    let(:country) { create :country }
    let(:user_params) do
      { email: "myEmail@example.com", password: "password", password_confirmation: "password",
     country_id: country.id }
    end

    it "registers new user" do
      post user_registration_path, params: user_params
      expect(response).to have_http_status(:ok)
    end
  end
end
