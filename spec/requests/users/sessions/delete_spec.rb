require "rails_helper"

RSpec.describe "User Session", type: :request do
  describe "DELETE /users/sign_out" do
    let(:user) { create :user }

    it "logs out user" do
      sign_in user
      delete destroy_user_session_path

      expect(response).to have_http_status(:found)
      expect(subject).to redirect_to root_path
    end
  end
end
