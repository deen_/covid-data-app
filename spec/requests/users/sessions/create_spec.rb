require "rails_helper"

RSpec.describe "User Session", type: :request do
  describe "POST /users/sign_in" do
    let(:user) { create :user }
    let(:user_params) do
      { user: { email: user.email, password: user.password } }
    end

    it "logs in user" do
      post user_session_path, params: user_params

      expect(response).to have_http_status(:found)
      expect(subject).to redirect_to root_path
    end
  end
end
