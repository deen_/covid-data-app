require "rails_helper"

RSpec.describe Users::RegistrationsController, type: :controller do
  describe "#create" do
    let(:country) { create :country }
    let(:user_params) do
      { email: "myEmail@example.com", password: "password", password_confirmation: "password",
     country_id: country.id }
    end
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:user]
    end

    it "creates new user and redirects to homepage" do
      expect do
        post :create, params: { user: user_params }
      end.to change(User, :count).by(1)
      expect(subject).to redirect_to root_path
    end
  end
end
