require "rails_helper"

RSpec.describe Devise::SessionsController, type: :controller do
  describe "#create" do
    let(:user) { create :user }
    let(:user_params) do
      { user: { email: user.email, password: user.password } }
    end

    controller do
      def after_sign_in_path_for(resource, _arg)
        super resource
      end
    end

    it "logs in valid user" do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      post :create, params: { user: user_params }

      expect(response).to have_http_status(:ok)
      expect(controller.after_sign_in_path_for(user, 0)).to eq root_path
    end
  end
end
