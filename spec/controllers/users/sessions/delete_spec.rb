require "rails_helper"

RSpec.describe Devise::SessionsController, type: :controller do
  describe "#destroy" do
    let(:user) { create :user }

    it "logs out authenticated user" do
      sign_in user
      @request.env["devise.mapping"] = Devise.mappings[:user]
      post :destroy

      expect(response).to have_http_status(:found)
      expect(subject).to redirect_to root_path
    end
  end
end
