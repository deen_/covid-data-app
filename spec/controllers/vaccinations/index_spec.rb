require "rails_helper"

RSpec.describe VaccinationsController, type: :controller do
  describe "#index" do
    it "populates an array of articles" do
      vaccinations = create_list :vaccination, 5

      get :index

      expect(assigns(:vaccinations)).to eq(vaccinations)
    end

    it "renders the :index view" do
      get :index
      expect(response).to have_http_status(:ok)
      expect(subject).to render_template(:index)
    end
  end
end
