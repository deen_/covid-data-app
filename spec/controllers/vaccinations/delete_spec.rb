require "rails_helper"

RSpec.describe VaccinationsController, type: :controller do
  describe "#delete" do
    let(:user) { create :user }
    let!(:vaccinations) { create_list :vaccination, 5 }

    context "as an authenticated user" do
      it "deletes csv uploads" do
        sign_in user

        delete :destroy
        expect(CsvUpload.count).to eq 0
      end
    end

    context "as a guest" do
      it "redirects to sign-in page" do
        delete :destroy
        expect(response).to have_http_status :found # 302
        expect(subject).to redirect_to "/users/sign_in"
      end
    end
  end
end
