require "rails_helper"

RSpec.describe VaccinationsController, type: :controller do
  describe "#create" do
    let(:user) { create :user }
    let(:vaccination_params) do
      { vaccination: { file: TestCSV.test_csv } }
    end

    context "as an authenticated user" do
      it "uploads CSV file and redirects to root path" do
        sign_in user

        expect do
          post :create, params: vaccination_params
        end.to change(ActiveStorage::Attachment, :count).by(1)
        expect(CsvUpload.count).to eq 1
        expect(subject).to redirect_to root_path
      end
    end

    context "as a guest" do
      it "redirects to sign-in page" do
        post :create, params: vaccination_params
        expect(response).to have_http_status :found # 302
        expect(subject).to redirect_to "/users/sign_in"
      end
    end
  end
end
