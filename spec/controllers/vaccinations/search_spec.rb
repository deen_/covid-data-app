require "rails_helper"

RSpec.describe VaccinationsController, type: :controller do
  let(:country_one) { create :country }
  let(:country_two) { create :country }
  let(:today) { Time.zone.now }
  let(:yesterday) { 1.day.ago }
  let(:vaccine_one) { create :vaccine }
  let(:vaccine_two) { create :vaccine }

  describe "#search" do
    it "can search by country" do
      create_list(:vaccination, 3, country_id: country_one.id)
      create_list(:vaccination, 2, country_id: country_two.id)

      search_params = { vaccination: { country: country_one.id } }
      post :search, params: search_params
      expect(assigns(:vaccinations).count).to eq 3
    end

    it "can search by data source" do
      create_list(:vaccination, 3, data_source: 0)
      create_list(:vaccination, 2, data_source: 1)

      search_params = { vaccination: { data_source: 0 } }
      post :search, params: search_params
      expect(assigns(:vaccinations).count).to eq 3
    end

    it "can search by date updated" do
      create_list(:vaccination, 3, date_updated: today)
      create_list(:vaccination, 2, date_updated: yesterday)

      search_params = { vaccination: { date_updated: today } }
      post :search, params: search_params
      expect(assigns(:vaccinations).count).to eq 3
    end

    it "can search by date first vaccinated" do
      create_list(:vaccination, 3, first_vaccine_date: today)
      create_list(:vaccination, 2, first_vaccine_date: yesterday)

      search_params = { vaccination: { first_vaccine_date: today } }
      post :search, params: search_params
      expect(assigns(:vaccinations).count).to eq 3
    end

    it "can search by vaccine" do
      create_list(:vaccination, 3, :with_vaccines, vaccine: vaccine_one)
      create_list(:vaccination, 2, :with_vaccines, vaccine: vaccine_two)

      search_params = { vaccination: { vaccination_vaccine: { vaccine: vaccine_one.id } } }
      post :search, params: search_params
      expect(assigns(:vaccinations).count).to eq 3
    end

    it "can search by combinations of multiple filter fields" do
      create_list(
        :vaccination,
        3,
        :with_vaccines,
        country_id:         country_one.id,
        data_source:        0,
        date_updated:       today,
        first_vaccine_date: today,
        vaccine:            vaccine_one
      )
      create_list(
        :vaccination,
        2,
        :with_vaccines,
        country_id:         country_two.id,
        data_source:        1,
        date_updated:       yesterday,
        first_vaccine_date: yesterday,
        vaccine:            vaccine_two
      )

      search_params = {
        vaccination: {
          country:             country_one.id,
          data_source:         0,
          date_updated:        today,
          first_vaccine_date:  today,
          vaccination_vaccine: { vaccine: vaccine_one.id }
        }
      }
      post :search, params: search_params
      expect(assigns(:vaccinations).count).to eq 3
    end
  end
end
