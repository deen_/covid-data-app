require "rails_helper"

RSpec.describe VaccinationsController, type: :controller do
  describe "#new" do
    context "as an authenticated user" do
      let(:user) { create :user }

      it "renders the :new view" do
        sign_in user
        get :new

        expect(response).to have_http_status(:ok)
        expect(subject).to render_template(:new)
      end
    end

    context "as a guest" do
      it "redirects to sign-in page" do
        get :new
        expect(response).to have_http_status :found # 302
        expect(subject).to redirect_to "/users/sign_in"
      end
    end
  end
end
