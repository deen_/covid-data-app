FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "myemail#{n}@example.com" }
    password { "password" }
    password_confirmation { "password" }

    country
  end
end
