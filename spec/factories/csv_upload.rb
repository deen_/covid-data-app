FactoryBot.define do
  factory :csv_upload do
    user
    after(:build) do |c|
      c.csv.attach(
        io: File.open(Rails.root.join("spec/fixtures/sample.csv")),
        filename: "sample.csv", content_type: "txt/csv"
      )
    end
  end
end
