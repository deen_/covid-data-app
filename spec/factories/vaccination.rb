FactoryBot.define do
  factory :vaccination do
    data_source { [0, 1].sample }
    date_updated { rand_time(2.days.ago) }
    total_vaccinations { rand(1..10) }
    persons_vaccinated_1plus_dose { rand(1..10) }
    total_vaccinations_per100 { rand(1..10) }
    persons_vaccinated_1plus_dose_per100 { rand(1..10) }
    first_vaccine_date { rand_time(5.days.ago) }
    number_vaccines_types_used { rand(1..10) }

    country

    trait :with_vaccines do
      transient do
        vaccine { "Sinovac" }
      end
      after(:build) do |vaccination, evaluator|
        vaccination.vaccination_vaccines << build(:vaccination_vaccine,
                                                  vaccine:     evaluator.vaccine,
                                                  vaccination: vaccination)
      end
    end
  end
end

def rand_time(from, to = Time.zone.now)
  Time.zone.at(rand_in_range(from.to_f, to.to_f))
end

def rand_in_range(from, to)
  rand * (to - from) + from
end
