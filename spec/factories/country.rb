FactoryBot.define do
  factory :country do
    sequence(:name) { |n| "Country #{n}" }
    sequence(:iso3) { |n| "ISO3 #{n}" }
    who_region { "XYZ" }
  end
end
