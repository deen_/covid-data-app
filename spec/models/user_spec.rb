# frozen_string_literal: true

require "rails_helper"

RSpec.describe User, type: :model do
  let(:country) { create :country }

  it "is valid with email, password, password confirmation and country" do
    user = User.new(
      email:                 "deen@example.com",
      password:              "password",
      password_confirmation: "password",
      country_id:            country.id
    )

    expect(user).to be_valid
  end
  it "is invalid without an email address" do
    user = User.new(
      password:   "password",
      country_id: country.id
    )

    expect(user).to_not be_valid
  end
end
