# frozen_string_literal: true

require "rails_helper"

RSpec.describe Country, type: :model do
  it "is valid with name, iso3 and who_region" do
    country = Country.new(
      name:       "Philippines",
      iso3:       "PHL",
      who_region: "WPRO"
    )

    expect(country).to be_valid
  end
end
