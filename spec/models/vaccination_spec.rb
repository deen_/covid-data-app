# frozen_string_literal: true

require "rails_helper"

RSpec.describe Vaccination, type: :model do
  let(:country) { create :country }

  it "is valid with country, data source, ..." do
    vaccination = Vaccination.new(
      country_id:                           country.id,
      data_source:                          "REPORTING",
      date_updated:                         Time.zone.now,
      total_vaccinations:                   1,
      persons_vaccinated_1plus_dose:        1,
      total_vaccinations_per100:            1,
      persons_vaccinated_1plus_dose_per100: 1,
      first_vaccine_date:                   Time.zone.now,
      number_vaccines_types_used:           1
    )

    expect(vaccination).to be_valid
  end
end
