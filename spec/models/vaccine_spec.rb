# frozen_string_literal: true

require "rails_helper"

RSpec.describe Vaccine, type: :model do
  it "is valid with name" do
    vaccine = Vaccine.new(
      name: "AstraZeneca - AZD1222"
    )

    expect(vaccine).to be_valid
  end
end
