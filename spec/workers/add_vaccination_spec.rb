require "rails_helper"
require "sidekiq/testing"
Sidekiq::Testing.fake!

RSpec.describe AddVaccination, type: :worker do
  describe "testing worker" do
    let(:amazon_url) do
      "https://covid-app-csv.s3.us-east-2.amazonaws.com/sample.csv"
    end

    it "goes into the jobs array for testing environment" do
      expect do
        described_class.perform_async(amazon_url)
      end.to change(described_class.jobs, :size).by(1)
    end

    it "saves vaccination data" do
      described_class.perform_async(amazon_url)
      Sidekiq::Worker.drain_all
      expect(Vaccination.count).to eq(3)
    end
  end
end
