require "rails_helper"
require "sidekiq/testing"
Sidekiq::Testing.fake!

RSpec.describe RemoveVaccination, type: :worker do
  describe "testing worker" do
    it "goes into the jobs array for testing environment" do
      expect do
        described_class.perform_async
      end.to change(described_class.jobs, :size).by(1)
    end

    it "deletes vaccination data" do
      create_list :vaccination, 5

      described_class.perform_async
      Sidekiq::Worker.drain_all
      expect(Vaccination.count).to eq(0)
    end
  end
end
