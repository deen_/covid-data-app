module TestCSV
  def self.test_csv
    Rack::Test::UploadedFile.new(Rails.root.join("spec/fixtures/sample.csv"), "text/csv")
  end
end

RSpec.configure do |c|
  c.include TestCSV
end
