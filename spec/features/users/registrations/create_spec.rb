require "rails_helper"

RSpec.feature "Users::Registrations::Creates", type: :feature do
  before(:each) do
    create :country, name: "Philippines"
  end

  scenario "registers new user" do
    visit root_path
    expect(page).to have_content "Register"

    click_link "Register"
    fill_in "Email", with: "myemail@example.com"
    select "Philippines", from: "user[country_id]"
    fill_in "Password", with: "password"
    fill_in "Password confirmation", with: "password"
    click_button "Sign up"

    expect(page).to have_content "Welcome! You have signed up successfully."
  end
end
