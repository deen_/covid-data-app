require "rails_helper"

RSpec.feature "Users::Sessions::Deletes", type: :feature do
  let(:user) { create :user }

  scenario "user logs out" do
    login_as user

    visit root_path
    expect(page).to have_content "Logout"

    click_link "Logout"
    expect(page).to have_content "Signed out successfully."
  end
end
