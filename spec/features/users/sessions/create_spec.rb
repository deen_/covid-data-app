require "rails_helper"

RSpec.feature "Users::Sessions::Creates", type: :feature do
  let(:user) { create :user }

  scenario "user logs in" do
    visit user_session_path
    fill_in "Email", with: user.email
    fill_in "Password", with: user.password

    click_button "Log in"
    expect(page).to have_content "Signed in successfully."
  end
end
