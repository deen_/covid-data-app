require "rails_helper"

RSpec.feature "Vaccination::Indices", type: :feature do
  let!(:vaccinations) { create_list :vaccination, 5 }

  scenario "renders vaccination records", js: true do
    visit root_path

    expect(page).to have_text("Vaccinations Record")
    within("tbody") do
      expect(page).to have_xpath(".//tr", count: 5)
    end
  end
end
