require "rails_helper"

RSpec.feature "Vaccinations::Deletes", type: :feature do
  let(:user) { create :user }
  let!(:vaccinations) { create_list :vaccination, 5 }

  scenario "deletes vaccination data", js: true do
    login_as user
    visit root_path

    accept_alert do
      click_link "Delete All Records"
    end
    expect(ActiveStorage::Attachment.count).to eq 0
  end
end
