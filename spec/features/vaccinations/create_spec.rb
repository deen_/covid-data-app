require "rails_helper"

RSpec.feature "Vaccinations::Creates", type: :feature do
  let(:user) { create :user }

  scenario "uploads csv file", js: true do
    login_as user
    visit root_path

    expect do
      click_link "Upload Record"
      attach_file("vaccination_file", Rails.root.join("spec/fixtures/sample.csv"))
      click_button "Upload CSV File"
    end.to change(ActiveStorage::Attachment, :count).by(1)
  end
end
