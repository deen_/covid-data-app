require "rails_helper"

RSpec.feature "Vaccinations::Searches", type: :feature do
  let(:today) { Time.zone.now }
  let(:yesterday) { 1.day.ago }
  let!(:to_search) do
    create(
      :vaccination,
      :with_vaccines,
      country_id:         (create :country).id,
      data_source:        0,
      date_updated:       today,
      first_vaccine_date: today,
      vaccine:            (create :vaccine)
    )
  end
  before(:each) do
    create_list(
      :vaccination,
      5,
      data_source:        1,
      date_updated:       yesterday,
      first_vaccine_date: yesterday
    )

    visit root_path
  end

  scenario "searches by country", js: true do
    select to_search.country.name, from: "vaccination[country]"
    click_button "SEARCH"

    within("tbody") do
      expect(page).to have_xpath(".//tr", count: 1)
      expect(page).to have_text(to_search.country.name)
    end
  end

  scenario "searches by data source", js: true do
    select to_search.data_source, from: "vaccination[data_source]"
    click_button "SEARCH"

    within("tbody") do
      expect(page).to have_xpath(".//tr", count: 1)
      expect(page).to have_text(to_search.data_source)
    end
  end

  scenario "searches by date updated", js: true do
    page.execute_script(
      "document.querySelector('#vaccination_date_updated').value = '#{to_search.date_updated}'"
    )
    click_button "SEARCH"

    within("tbody") do
      expect(page).to have_xpath(".//tr", count: 1)
      expect(page).to have_text(to_search.date_updated)
    end
  end

  scenario "searches by date first vaccinated", js: true do
    page.execute_script(
      "document.querySelector('#vaccination_first_vaccine_date').value =
        '#{to_search.first_vaccine_date}'"
    )
    click_button "SEARCH"

    within("tbody") do
      expect(page).to have_xpath(".//tr", count: 1)
      expect(page).to have_text(to_search.first_vaccine_date)
    end
  end

  scenario "searches by vaccine", js: true do
    vaccine = to_search.vaccination_vaccines.first.vaccine.name

    select vaccine, from: "vaccination[vaccination_vaccine][vaccine]"
    click_button "SEARCH"

    within("tbody") do
      expect(page).to have_xpath(".//tr", count: 1)
      expect(page).to have_text(vaccine)
    end
  end

  scenario "searches by combinations of multiple filter fields", js: true do
    vaccine = to_search.vaccination_vaccines.first.vaccine.name

    select to_search.country.name, from: "vaccination[country]"
    select to_search.data_source, from: "vaccination[data_source]"
    page.execute_script(
      "document.querySelector('#vaccination_date_updated').value =
        '#{to_search.date_updated}'"
    )
    page.execute_script(
      "document.querySelector('#vaccination_first_vaccine_date').value =
        '#{to_search.first_vaccine_date}'"
    )
    select vaccine, from: "vaccination[vaccination_vaccine][vaccine]"
    click_button "SEARCH"

    within("tbody") do
      expect(page).to have_xpath(".//tr", count: 1)
      expect(page).to have_text(to_search.country.name)
      expect(page).to have_text(to_search.data_source)
      expect(page).to have_text(to_search.date_updated)
      expect(page).to have_text(to_search.first_vaccine_date)
      expect(page).to have_text(vaccine)
    end
  end
end
